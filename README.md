<!-- Intro Scetion -->
<h1> Hi there, <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="40px"> I'm </h2>
<p>
<a href="https://github.com/pritambera2000" target="_blank" >
<img height="45px" src="https://img.shields.io/badge/-P%20R%20I%20T%20A%20M-blue?style=for-the-badge" alt=""  >
<img height="45px"  src="https://img.shields.io/badge/-B%20E%20R%20A-pink?style=for-the-badge" alt="">
</a>
<img align="right" width="400" src="https://raw.githubusercontent.com/pritambera2000/pritambera2000/main/assets/blogging.svg" />
<h4> Self-taught developer with a keen interest in Linux</h4>
<!-- <img align = "right" width="400" src="./assets/web development.svg" /> -->
</p>

<!-- About Me Section -->
<p align="left" >

<h2>About Me 🚀</h2>

- 🎓 I’m currently pursuing my Bachelors Degree in Zoology
- 🌱 Currently learning Basic JavaScript & BootStrap
- 🐧 learning Linux fundamentals
- 💻 Learning Basics Of Networking & CyberSecurity @TryHackMe
</p>
<!-- Skills Section -->
<h2>Technical Skills 🛠</h2>
<p>

![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=for-the-badge&logo=javascript&logoColor%23F7DF1E)
![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)
![BootStrap](https://img.shields.io/badge/-Bootstrap-%238A12FC?style=for-the-badge&logo=bootstrap&logoColor=white)
![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)
![Vim](https://img.shields.io/badge/VIM-%2311AB00.svg?style=for-the-badge&logo=vim&logoColor=white)
![Shell Script](https://img.shields.io/badge/BASH-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)
</p>

<!-- Profile Stats -->

# Blogs 👨‍💻
### Read My Latest Blogs Here 👇

<!-- BLOG-POST-LIST:START -->
- [How To Connect a Remote Server Via SSH](https://dev.to/pritambera2000/how-to-connect-a-remote-server-via-ssh-3i91)
- [Beginners Guide for Getting Started with Git & GitHub](https://pritambera2000.hashnode.dev/beginners-guide-for-getting-started-with-git-and-github) 
<!-- BLOG-POST-LIST:END -->

<!-- <a href="https://dev.to/pritambera2000" target="_blank">
    <img src="https://img.shields.io/badge/-dev-black?style=for-the-badge&logo=dev.to" alt="Pritam Bera | dev.to">
</a> -->

<!-- Socials -->

## Get In Touch 👇

<p>
<a href="https://pritambera2000.github.io/portfolio/ "target="_blank">
    <img src="https://img.shields.io/badge/-Potfolio-%23ff6685?style=for-the-badge&logo=Opsgenie" alt="Pritam Bera | Portfolio">
</a>
<!-- <a href="">
    <img src="https://img.shields.io/badge/-GitHub-black?style=for-the-badge&logo=GitHub" alt="">
</a> -->
<a href="https://github.com/pritambera2000" target="_blank">
    <img src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white"alt="Pritam Bera | GitHub">
</a>
<a href="https://pritambera2000.hashnode.dev/" target="_blank">
    <img src="https://img.shields.io/badge/-HASHNODE-%232962FF?style=for-the-badge&logo=hashnode&logoColor=white&logoWidth=17" alt="Pritam Bera | Hashnode">
</a>
<a href="https://instagram.com/pritamlovesphotography" target="_blank">
    <img src="https://img.shields.io/badge/-INSTAGRAM-%09%23e1306c?style=for-the-badge&logo=instagram&&logoColor=white&logoWidth=17" alt="Pritam Bera | Instagram">
</a>
<a href="https://twitter.com/dotslashpritam" target="_blank">
    <img src="https://img.shields.io/badge/-TWITTER-%09%231DA1F2?style=for-the-badge&logo=twitter&logoColor=white&logoWidth=17" alt="Pritam Bera | Twitter">
</a>
<a href="https://dev.to/pritambera2000" target="_blank">
    <img src="https://img.shields.io/badge/-dev-black?style=for-the-badge&logo=dev.to" alt="Pritam Bera | dev.to">
</a>
<a href="">
    <img src="https://img.shields.io/badge/-LINKEDIN-blue?style=for-the-badge&logo=linkedin" alt="">
</a>

<br>
<br>

</p>
<!-- TeleGram & Mail Link -->

</p>
<a href="mailto:dev.pritambera@pm.me" target="_blank">
  <img align="right" alt="Pritam Bera | ProtonMail" src="https://img.shields.io/badge/-Private-green?style=social&logo=protonmail" />
</a>
<a href="https://t.me/dotslashpritam" target="_blank">
  <img align="right" alt="Pritam Bera | Telegram" src="https://img.shields.io/badge/-Personal-green?style=social&logo=telegram" />
</a>
<a href="https://open.spotify.com/playlist/3sK9eJPfx7zQgHjgnreUrJ?si=8vqNthxGRYuVDbpiNkis3g&utm_source=whatsapp" target="_blank">
  <img align="right" alt="Pritam Bera | Telegram" src="https://img.shields.io/badge/-ListenPritam-green?style=social&logo=spotify" />
</a>

<br>
